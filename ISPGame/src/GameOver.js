var GameOver = cc.LayerColor.extend({
	
	init : function() {
		this._super();
		this.BackGround = new GameOverBackground();
		this.BackGround.setPosition(screenWidth/2,screenHeight/2);
		this.timeSurvive = 0;
		this.timeLabel = cc.LabelTTF.create(this.timeSurvive + " Seconds", 'Arial', 40 );
		this.timeLabel.setPosition(new cc.Point(screenWidth/2,screenHeight/2-30))
		this.addChild(this.BackGround);
		this.addChild(this.timeLabel);
		this.addKeyboardHandlers();
		this.scheduleUpdate();
	},

	update : function() {
		this.playBGM();
	},

	setTimed : function(newTime) {
		this.timeSurvive = newTime;
		this.timeLabel.setString(this.timeSurvive + " Seconds");
	},

	addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );

            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },

    playBGM : function() {
    	cc.audioEngine.playMusic(res.GameOverBGM_wav);
    },

    onKeyDown : function(){

    },

    onKeyUp : function() {
    	this.unscheduleUpdate();
    	cc.director.runScene(new MainScene());
    }




});

var GameOverScene = cc.Scene.extend({
onEnter: function(time){
	this._super();
	this.timePassed = time;
	var over = new GameOver();
	over.init();
	over.setTimed(this.timePassed);
	this.addChild(over);
	}
})