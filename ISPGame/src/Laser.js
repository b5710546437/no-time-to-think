var Laser = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile(res.Laser_png);

		this.isStart = false;
		this.isPlaySound = false;
		this.speed = Laser.INIT_VELOCITY;
		this.degree = Math.floor(Math.random()*360)+1;
		this.setRotation(this.degree);
	},

	started : function() {
		this.isStart = true;
	},

	update : function() {
		if(this.isStart){
			this.isBounce();
			this.move();
			this.schedule(this.increaseSpeed,10);
			if(this.isPlaySound){
				this.isPlaySound = false;
				cc.audioEngine.playEffect(res.LaserBounceEffect_wav);
			}

		}
	},

	move : function() {
		var pos = this.getPosition();
		this.setPosition(new cc.Point(pos.x+this.speed*Math.sin(this.degree/180.0*Math.PI),pos.y+this.speed*Math.cos(this.degree/180.0*Math.PI)));
	},

	isHitBound : function(x,y) {
		if(x>screenWidth)
			return 4;
		else if(x<0)
			return 3;
		else if(y>screenHeight)
			return 1;
		else if(y<0)
			return 2;
	},

	isBounce : function() {
		var pos = this.getPosition();
		var keepSpeed = this.speed;
		this.speed = Laser.BF_BOUNCE;
		if(this.isHitBound(pos.x,pos.y)==Laser.BOUND.RIGHT){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(180>this.degree)
			this.isPlaySound = true;
		}
		else if(this.isHitBound(pos.x,pos.y)==Laser.BOUND.LEFT){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(180<this.degree)
			this.isPlaySound = true;
		}
		else if(this.isHitBound(pos.x,pos.y)==Laser.BOUND.UP){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(270<this.degree&&this.degree<90)
			this.isPlaySound = true;
		}
		else if(this.isHitBound(pos.x,pos.y)==Laser.BOUND.DOWN){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(270>this.degree&&this.degree>90)
			this.isPlaySound = true;
		}
		this.setRotation(this.degree);
		this.speed = keepSpeed;
		
	},

	bounceByLaser : function(bound) {
		var keepSpeed = this.speed;
		this.speed = Laser.BF_BOUNCE;
		if(bound==Laser.BOUND.RIGHT){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(180>this.degree)
		}
		else if(bound==Laser.BOUND.LEFT){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(180<this.degree)
		}
		else if(bound==Laser.BOUND.UP){
			 do{
				this.degree = Math.floor(Math.random()*360)+1;
			 }while(270<this.degree&&this.degree<90)
		}
		else if(bound==Laser.BOUND.DOWN){
			do{
				this.degree = Math.floor(Math.random()*360)+1;
			}while(270>this.degree&&this.degree>90)
		}
		this.setRotation(this.degree);
		this.speed = keepSpeed;

	},

	increaseSpeed : function(){
		if(this.speed<Laser.MAX_VELOCITY)
			this.speed++;
	},

	isOutOfBound : function(x,y){
		if(x>screenWidth+20||x<-20||y>screenHeight+20||y<-20)
			return true;
		return false;
	},

	




});

Laser.BOUND = {
	UP : 1,
	DOWN : 2,
	LEFT : 3,
	RIGHT : 4
}
Laser.STATUS = {
	WIDTH : 72,
	HEIGHT : 72
}
Laser.INIT_VELOCITY = 3;
Laser.MAX_VELOCITY = 10;
Laser.BF_BOUNCE = 0.5;