var Player = cc.Sprite.extend({
	ctor : function() {
		this._super();
		this.initWithFile(res.Player_png);
		
		//checking and init status
		this.isStart = false;
		this.isLeft = false;
		this.isRight = false;
		this.isUp = false;
		this.isDown = false;
		this.isJustBounce = false;
		this.isJustDead = false;


		this.blinkDistance = Player.BLINK_DISTANCE;
		this.direction = Player.DIR.UP;
		this.speed = Player.START_VELOCITY;

	},

	started : function() {
		this.isStart = true;
	},

	update : function() {
		if(this.started){
			var pos = this.getPosition();
			this.isBound(pos.x,pos.y);	
			this.walking();
		}
	},

	acceleration : function() {
		this.speed=Player.MIN_VELOCITY;
	},

	deceleration : function() {
		if(this.speed>0)
			this.speed-=1;
		else if(this.speed<0)
			this.speed+=1;
		

	},

	blinked : function () {
		var pos = this.getPosition();
		if ( this.direction == Player.DIR.UP ) {
	    	this.setPosition(new cc.Point(pos.x,pos.y + this.blinkDistance));
		}
		else if(this.direction == Player.DIR.DOWN){
			this.setPosition(new cc.Point(pos.x,pos.y - this.blinkDistance));
		}
		else if(this.direction == Player.DIR.RIGHT){
			this.setPosition(new cc.Point(pos.x + this.blinkDistance,pos.y));
		}
		else if(this.direction == Player.DIR.LEFT){
			this.setPosition(new cc.Point(pos.x - this.blinkDistance,pos.y));
		}
		
	},

	walking : function() {
		var pos = this.getPosition();
		if ( this.direction == Player.DIR.UP ) {
	    	this.setPosition(new cc.Point(pos.x,pos.y + this.speed));
		}
		else if(this.direction == Player.DIR.DOWN){
			this.setPosition(new cc.Point(pos.x,pos.y - this.speed));
		}
		else if(this.direction == Player.DIR.RIGHT){
			this.setPosition(new cc.Point(pos.x + this.speed,pos.y));
		}
		else if(this.direction == Player.DIR.LEFT){
			this.setPosition(new cc.Point(pos.x - this.speed,pos.y));
		}
	},

	switchDirection : function(direction) {
		this.direction = direction;
		if ( this.direction == Player.DIR.UP ) 
	    	this.setRotation(0);
		else if(this.direction == Player.DIR.DOWN)
			this.setRotation(180);
		else if(this.direction == Player.DIR.RIGHT)
			this.setRotation(90);
		else if(this.direction == Player.DIR.LEFT)
			this.setRotation(270);
	},

	isBound : function(x,y){
		if(x>screenWidth||x<0||y>screenHeight||y<0)
			this.bounce();
	},

	canDeceleration : function(){
		return (this.isUp==false&&this.isDown==false&&this.isLeft==false&&this.isRight==false)||this.isJustBounce;
	},

	bounce : function(){
		this.speed=Player.BOUNCE_SPEED;
		this.isJustBounce = true;
	},

	dead : function() {
		cc.audioEngine.playEffect(res.DeadEffect_wav);
		this.setPosition(new cc.Point(-1000,-1000))
		this.removeFromParent();
	},

	isOutOfBound : function(x,y){
		if(x>screenWidth+20||x<-20||y>screenHeight+20||y<-20)
			return true;
		return false

	}




});
Player.DIR = {
	UP : 1,
	DOWN : 2,
	LEFT : 3,
	RIGHT : 4
}
Player.CODE = {
	UP : 38,
	DOWN : 40,
	LEFT : 37,
	RIGHT : 39
}
Player.STAT = {
	WIDTH : 30,
	HEIGHT : 44
}
Player.START_VELOCITY = 0;
Player.MIN_VELOCITY = 7;
Player.BRAKE_SPEED = 1;
Player.ACCELERATION = 1;
Player.BOUNCE_SPEED = -10;
Player.BLINK_DISTANCE = 100;

