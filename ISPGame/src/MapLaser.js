var MapLaser = cc.Sprite.extend({

	ctor : function(dir,ran) {
		this._super();
		//this.direction = Math.floor(Math.random()*4)+1;
		this.direction = dir;
		this.ran = ran;
		this.degree = this.rotation();
		//this.ran = Math.floor(Math.random()*2)+1;
		this.set();
		this.speed = MapLaser.INIT_VELOCITY;

	},

	update : function() {
		this.move();
		this.outOfBound();
	},

	move : function() {
		var pos = this.getPosition();
		this.setPosition(new cc.Point(pos.x+this.speed*Math.sin(this.degree/180.0*Math.PI),pos.y+this.speed*Math.cos(this.degree/180.0*Math.PI)));
	},

	rotation : function() {
		if(this.direction == MapLaser.DIR.VERTICAL)
		{
			if(this.ran == MapLaser.RAN.RIGHT){
				this.setRotation(0);
				return 90;
			}
			else if(this.ran == MapLaser.RAN.LEFT){
				this.setRotation(180);
				return 270;
			}
		}
		else if(this.direction == MapLaser.DIR.HORIZONTAL)
		{
			if(this.ran == MapLaser.RAN.UP){
				this.setRotation(90);
				return 0;
			}
			else if(this.ran == MapLaser.RAN.DOWN){
				this.setRotation(270);
				return 180;
			}
			
		}
	
	},

	set : function() {
		
		if(this.direction == MapLaser.DIR.VERTICAL)
		{
			if(this.ran == MapLaser.RAN.RIGHT)
				this.setPosition(new cc.Point(40,screenHeight/2));
			else if(this.ran == MapLaser.RAN.LEFT)
				this.setPosition(new cc.Point(screenWidth-40,screenHeight/2));
		}
		else if(this.direction == MapLaser.DIR.HORIZONTAL)
		{
			if(this.ran == MapLaser.RAN.UP)
				this.setPosition(new cc.Point(screenWidth/2,40));
			else if(this.ran == MapLaser.RAN.DOWN)
				this.setPosition(new cc.Point(screenWidth/2,screenHeight-40));
		}
		
	},

	isHitPlayer : function(playerPos) {
		var mapLaserPos = this.getPosition();
		if(this.direction == MapLaser.DIR.VERTICAL)
			return Math.abs(playerPos.x-mapLaserPos.x)<MapLaser.STAT.WIDTH;
		else if(this.direction == MapLaser.DIR.HORIZONTAL)
			return Math.abs(playerPos.y-mapLaserPos.y)<MapLaser.STAT.WIDTH;
		
	},

	isHitLaser : function(laserPos) {
		var mapLaserPos = this.getPosition();
		if(this.direction == MapLaser.DIR.VERTICAL)
			return Math.abs(laserPos.x-mapLaserPos.x)<MapLaser.STAT.WIDTH;
		else if(this.direction == MapLaser.DIR.HORIZONTAL)
			return Math.abs(laserPos.y-mapLaserPos.y)<MapLaser.STAT.WIDTH;
	},

	outOfBound : function(){
		var mapLaserPos = this.getPosition();
		if(mapLaserPos.x>screenWidth||mapLaserPos.x<0||mapLaserPos.y>screenHeight||mapLaserPos.y<0){
			this.removeFromParent();
		}
	}
});

MapLaser.DIR = {
	VERTICAL : 1,
	HORIZONTAL : 2
}
MapLaser.RAN = {
	UP : 1,
	DOWN : 2,
	LEFT : 3,
	RIGHT : 4
}

MapLaser.INIT_VELOCITY = 1;
MapLaser.STAT = {
	WIDTH : 16,
	HEIGHT : 1376
}