var GameTitle = cc.Sprite.extend({
    ctor: function() {
        this._super();
   
        var animation = new cc.Animation.create();
    	animation.addSpriteFrameWithFile( res.Title1_png );
		animation.addSpriteFrameWithFile( res.Title2_png );
		animation.addSpriteFrameWithFile( res.Title3_png );
    	animation.addSpriteFrameWithFile( res.Title4_png );
    	animation.addSpriteFrameWithFile( res.Title5_png );
    	animation.addSpriteFrameWithFile( res.Title6_png );
    	animation.addSpriteFrameWithFile( res.Title5_png );
		animation.addSpriteFrameWithFile( res.Title4_png );
		animation.addSpriteFrameWithFile( res.Title3_png );
    	animation.addSpriteFrameWithFile( res.Title2_png );
    	animation.addSpriteFrameWithFile( res.Title1_png );
		animation.setDelayPerUnit( 0.05 );

		var action = cc.RepeatForever.create( cc.Animate.create( animation ) );
		this.runAction( action );
	}
});