var Howtoplay =  cc.LayerColor.extend({
	
	init : function(){
		this._super(new cc.Color(127,127,127,255));
		this.setPosition(new cc.Point(0,0));
		this.Background = new HTPBackground();
		this.Background.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
		this.addChild(this.Background);
		this.addKeyboardHandlers();
		this.scheduleUpdate();
	},

	update : function() {
		this.playBGM();
	},

	playBGM : function() {
		cc.audioEngine.playMusic(res.MainBackground_wav,true);
	},

	stopBGM : function() {
		cc.audioEngine.stopMusic();
	},
	
	addKeyboardHandlers: function() { 
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );

            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    
    onKeyDown : function (keyCode,event) {

    },

    onKeyUp : function (keyCode,event){
    	this.stopBGM();
		this.unscheduleUpdate();
		cc.director.runScene(new MainScene());
    }

});


var howToPlayScene = cc.Scene.extend({
onEnter: function(){
	this._super();
	var layer = new Howtoplay();
	layer.init();
	this.addChild(layer);
	}
})