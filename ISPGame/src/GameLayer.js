var GameLayer = cc.LayerColor.extend({
	
	init : function() {
		this._super(new cc.Color(127,127,127,255));
		this.setPosition(new cc.Point(0,0));
		
		this.createBackground();
		this.createPlayer();
		this.createLaser();

		this.borderLaserPresent = false;
		this.deadLaserPresent = false;
		
		this.timePassed = 0;
       
        this.laserSet = [];
		this.laserSet.push(this.laser)

		this.borderLaserSet = [];
		this.deadLaserSet = [];
	 
		this.addKeyboardHandlers();

		this.createLifeLabel();
		this.createTimeLabel();
		this.createStartLabel();
		
		this.scheduleUpdate();
		return true;
	},

	started : function() {
		this.isStarted = true;
		this.removeChild(this.startLabel);		
		this.player.started();
		this.laser.started();
		
	},

	update : function(dt) {
		
		if(this.isStarted){
			this.schedule(this.timeFly,1);
			if(this.player.canDeceleration())
				this.player.deceleration();
			if(this.borderLaserPresent&&!this.player.isjustDead)
				for(var i =0;i<this.borderLaserSet.length;i++)
					this.borderLaserSet[i].isHit(this.player,this.laserSet);
			if(this.deadLaserPresent&&!this.player.isjustDead)
				for(var i = 0;i<this.deadLaserSet.length;i++)
					if(this.deadLaserSet[i].isHit(this.player))
						this.isDead = true;
			this.checkHitLaser();
			this.schedule(this.increaseDiff,7);
			if(this.isDead){
				this.respawn();
				this.schedule(this.invisibility,3);
				this.isDead = false;
			}
			this.outOfBoundLaser();
			this.outOfBoundPlayer();
		}
		this.playBGM();
	},

	timeFly : function() {
		this.timePassed++;
		this.timeLabel.setString('Time : '+ this.timePassed);
	},

	createPlayer : function() {
		this.player = new Player();
		this.player.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
		this.life = GameLayer.Player.MAX_LIFE;
		this.isDead = false;
		this.addChild(this.player);
		this.player.scheduleUpdate();
	},

	createLaser : function() {
		this.laser = new Laser();
        this.laser.setPosition(new cc.Point(screenWidth/2,20));
        this.addChild(this.laser);
		this.laser.scheduleUpdate();
	},

	createBackground : function() {
		this.Background = new Background();
        this.Background.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
		this.addChild(this.Background);	
	},

	createLifeLabel : function() {
		this.lifeLabel = cc.LabelTTF.create('LIFE : '+this.life, 'Arial', 20 );
		this.lifeLabel.setPosition(new cc.Point(screenWidth-50,50));
		this.addChild(this.lifeLabel);
	},

	createTimeLabel : function() {
		this.timeLabel = cc.LabelTTF.create('Time : '+this.timePassed,'Arial',20);
		this.timeLabel.setPosition(new cc.Point(screenWidth-50,screenHeight-50));
		this.addChild(this.timeLabel);
	},

	createStartLabel : function() {
		this.startLabel = cc.LabelTTF.create('Press any key to start.','Arial',70);
		this.startLabel.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
		this.addChild(this.startLabel);
	},

	onKeyDown : function(keyCode,event) {
		this.started();
		if(!this.player.isJustBounce){
			if(keyCode == cc.KEY.up){
				this.player.isUp = true;
				this.player.switchDirection(1);
				this.player.acceleration();
			}
			else if(keyCode == cc.KEY.down){
				this.player.isDown = true;
				this.player.switchDirection(2);
				this.player.acceleration();
			}
			else if(keyCode == cc.KEY.left){
				this.player.isLeft = true;
				this.player.switchDirection(3);
				this.player.acceleration();
			}
			else if(keyCode == cc.KEY.right){
				this.player.isRight = true;
				this.player.switchDirection(4);
				this.player.acceleration();
			}
		}
	},

	onKeyUp : function(keyCode,event) {
		this.player.isJustBounce = false;
		if(keyCode == cc.KEY.up)
			this.player.isUp = false;
		
		else if(keyCode == cc.KEY.down)
			this.player.isDown = false;
		
		else if(keyCode == cc.KEY.left)
			this.player.isLeft = false;	
		
		else if(keyCode == cc.KEY.right)
			this.player.isRight = false;

		if(keyCode == cc.KEY.space)
            this.player.blinked();
		
	},

	addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );

            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },

    checkHitLaser : function() {
    	if(!this.player.isjustDead){
			var playerPos = this.player.getPosition();
			for(var i=0; i<this.laserSet.length;i++){
				var laserPos = this.laserSet[i].getPosition();
				if(this.isHitLaser(playerPos,laserPos)){
					this.player.initWithFile(res.DeadEffect_png);
					this.player.dead();
					this.isDead = true;
				}
			}
		}
		// else
		// 	this.schedule(this.invisibility,3);
	},

	invisibility : function() {
		this.player.isjustDead = false;

	},

	isHitLaser : function(playerPos,laserPos) {
		return Math.abs(playerPos.x-laserPos.x)<GameLayer.Player.WIDTH&&Math.abs(playerPos.y-laserPos.y)<GameLayer.Player.HEIGHT;
	},

	respawn : function() {
		if(this.life>0){
			this.player = new Player();
			this.player.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
			this.player.isjustDead = true;
			this.addChild(this.player);
			this.player.scheduleUpdate();
			this.life--;
			this.lifeLabel.setString('LIFE : '+this.life);
		}
		else
		{
			this.unscheduleUpdate();
			cc.director.runScene(new GameOverScene(this.timePassed));
		}
	},

	

	addLaser : function() {
		if(this.laserSet.length<GameLayer.MAX_LASER){
			var laser = new Laser()
			var degree = Math.floor(Math.random*360)+1;
			var posX = Math.floor(Math.random()*screenWidth)+1;
			var posY = Math.floor(Math.random()*screenHeight)+1;
			laser.setPosition(new cc.Point(posX,posY));
			laser.setRotation(degree);
			laser.started();
			this.laserSet.push(laser);
			this.addChild(laser);
			laser.scheduleUpdate();
			//return false;
			return true;
		}
		else
			return false;
		

	},
	
	increaseDiff : function() {
		if(!this.addLaser()){
			 this.addBorderLaser();
			 this.addDeadLaser();
		}
		
	},

	addBorderLaser : function() {
		var dir = Math.floor(Math.random()*2)+1;
		var ran = this.getMapLaserRan(dir);
		var borderLaser = new BorderLaser(dir,ran);
		borderLaser.scheduleUpdate();
		this.borderLaserSet.push(borderLaser);
		this.addChild(borderLaser);
		this.borderLaserPresent = true;
	},

	addDeadLaser : function() {
		var dir = Math.floor(Math.random()*2)+1;
		var ran = this.getMapLaserRan(dir);
		var deadLaser = new DeadLaser(dir,ran);
		deadLaser.scheduleUpdate();
		this.deadLaserSet.push(deadLaser);
		this.addChild(deadLaser);
		this.deadLaserPresent = true;
	},

	getMapLaserRan : function(dir) {
		var ran = Math.floor(Math.random()*2)+1;
		if(dir == 1&&ran==1)
			ran = 4;
		else if(dir == 1&&ran==2)
			ran = 3;
		else if(dir ==2&&ran==1)
			ran = 1;
		else if(dir ==2&&ran==2)
			ran = 2;
		return ran;

	},

	outOfBoundLaser : function() {
		for (var i = 0; i < this.laserSet.length; i++) {
			var laserPos = this.laserSet[i].getPosition();
			if(this.laserSet[i].isOutOfBound(laserPos.x,laserPos.y)){
				this.removeChild(this.laserSet[i]);
				this.laserSet[i] = new Laser();
				var degree = Math.floor(Math.random*360)+1;
				var posX = Math.floor(Math.random()*screenWidth)+1;
				var posY = Math.floor(Math.random()*screenHeight)+1;
				this.laserSet[i].setPosition(new cc.Point(posX,posY));
				this.laserSet[i].setRotation(degree);
				this.laserSet[i].started();
				this.addChild(this.laserSet[i]);
				this.laserSet[i].scheduleUpdate();
			}
		}
	},

	outOfBoundPlayer : function() {
		var playerPos = this.player.getPosition();
		if(this.player.isOutOfBound(playerPos.x,playerPos.y)){
			this.player.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
			this.isDead = true;
		}
	},

	playBGM : function() {
		cc.audioEngine.playMusic(res.BackgroundMusic_wav,true);
	}
	
});

GameLayer.Player = {
	WIDTH : 30,
	HEIGHT : 44,
	MAX_LIFE : 3
}

GameLayer.MAX_LASER = 4;



var StartScene = cc.Scene.extend({
onEnter: function(){
	this._super();
	var layer = new GameLayer();
	layer.init();
	this.addChild(layer);
	}
})





