var MainMenu = cc.LayerColor.extend({

	init : function() {
		this._super(new cc.Color(127,127,127,255));
		this.setPosition(new cc.Point(0,0));
		this.Background = new MainBackground();
	 	this.Background.setPosition(new cc.Point(screenWidth/2,screenHeight/2));
		this.addChild(this.Background);
		
		this.startButton = new cc.MenuItemImage(
			res.BeforeStartButton_png,
			res.AfterStartButton_png,
			function(){
				this.stopBGM();
				this.unscheduleUpdate();
				cc.director.runScene(new StartScene());
			},this);
		this.startButton = new cc.Menu(this.startButton);
		this.startButton.setPosition(screenWidth/2,screenHeight-300);
		this.addChild(this.startButton);

		this.howToPlayButton = new cc.MenuItemImage(
			res.BeforeHTPButton_png,
			res.AfterHTPButton_png,
			function(){
				this.stopBGM();
				this.unscheduleUpdate();
				cc.director.runScene(new howToPlayScene());
			},this);
		this.howToPlayButton = new cc.Menu(this.howToPlayButton);
		this.howToPlayButton.setPosition(screenWidth/2,screenHeight-500);
		this.addChild(this.howToPlayButton);

		this.gameTitle = new GameTitle();
		this.gameTitle.setPosition( new cc.p(screenWidth/2,screenHeight-700));
		this.addChild(this.gameTitle);
		
		this.playBGM();
		this.scheduleUpdate();
	},

	update : function() {
		this.playBGM();
	},

	playBGM : function(){
		cc.audioEngine.playMusic(res.MainBackground_wav,true);
	},

	stopBGM : function(){
		cc.audioEngine.stopMusic();
	}

});

var MainScene = cc.Scene.extend({
onEnter: function(){
	this._super();
	var menu = new MainMenu();
	menu.init();
	this.addChild(menu);
	}
})