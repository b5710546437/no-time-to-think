var res = {
    Player_png : "res/Player.png",
    Laser_png : "res/Laser_Edit.png",
    BorderLaser_png : "res/BorderLaser.png",
    Background_png : "res/Game_Background.png",
    DeadLaser_png : "res/DeadLaser.png",
    BackgroundMusic_wav : "res/Sound/BGM.wav",
    LaserBounceEffect_wav : "res/Sound/Laser_Bounce.wav",
    MainBackground_wav : "res/Sound/MainBGM.wav",
    GameOverBGM_wav : "res/Sound/GameOverBGM.wav",
    MainBackground_png : "res/Main_Background.png",
    HTPBackground_jpg : "res/HTP_Background.jpg",
    GameOverBackground_jpg : "res/GameOver_Background.jpg",
    BeforeStartButton_png : "res/BFStart.png",
    AfterStartButton_png : "res/AFStart.png",
    BeforeHTPButton_png : "res/BFHowtoplay.png",
    AfterHTPButton_png : "res/AFHowtoplay.png",
    Title1_png : "res/NoTimeToThink0.png",
    Title2_png : "res/NoTimeToThink1.png",
    Title3_png : "res/NoTimeToThink2.png",
    Title4_png : "res/NoTimeToThink3.png",
    Title5_png : "res/NoTimeToThink4.png",
    Title6_png : "res/NoTimeToThink5.png",
    DeadEffect_wav : "res/Sound/DeadEffect.wav",
    DeadEffect_png : "res/DeadEffect.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}